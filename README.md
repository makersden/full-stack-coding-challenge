# Coding Challenge

> Welcome to the Challenge!

## Overview

To complete this challenge, you will need to write a simple [React](https://facebook.github.io/react/) web app, and provide us the source files to be built.

The purpose of this challenge is to assess your **skills and approach to composing a simple web app and backend** given a set of screens and data. 
We will also assess the **generated HTML, CSS, and JS** output.


## The Challenge

It's pretty simple. Using the provided screens as a reference, you'll need to build a set of React components to render the app.  
You'll also need to implement a RESTful Node.js backend that serves a feed of data, filters that
data, and then use the relevant fields from it.

Although this is a basic exercise, we'll be looking for **simple, well-designed and tested code** in the submission.

Please include a `README` with setup instructions, and any tests or other documentation you created as part of your solution.

Also, add the following info to your `README`:

* How did you decide which technologies to use as part of your solution?
* Are there any improvements you could make to your submission?
* What would you do differently if you were allocated more time?

## Details

You will need to build the following 3 pages with React:

* A "Home" page
* A "Series" page
* A "Movies" page

Please create components for each part of the page (eg. header, content, footer, etc).
Assets are provided in the `assets` folder.

The pages should also be usable on mobile and tablet devices.

You can assume that you do not have to support legacy browsers without features such as `fetch` or `flexbox`.


### "Home" Page

Refer to the [screens/1-home.jpg](./screens/1-home.jpg) screen.

This will be your `index.html` screen.

You will need to display 2 tiles, which link to the "Series" page and the "Movies" page.


### "Series" and "Movies" Pages

Refer to the [screens/2-series.jpg](./screens/2-series.jpg) and [screens/3-movies.jpg](./screens/3-movies.jpg) screens.

For each page you will need to make the backend serve this JSON feed [feed/sample.json](https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json).
We'd like the backend to do:

* Paging of the collection (e.g. using query parameters)
* Changing the page size (10, 20, 50 items)
* Filtering by `releaseYear` (again, query param is ok)
* Sorting by `releaseYear` and `title` (both ascending and descending)

For paging and sorting, please implement a basic set of frontend components
allowing to control those features.

For the "Series" page filter on:

* Where the entry has a `programType` attribute value of `series`

For the "Movies" page filter on:

* Where the entry has a `programType` attribute value of `movie`

The attributes you should use to display the entries are:

* `title`
* `images` → `Poster Art` → `url`

You will also need to handle the loading and error states, of fetching the JSON feed:

* "Loading" state [screens/1.1-loading.jpg](./screens/1.1-loading.jpg)
* "Error" state [screens/1.2-error.jpg](./screens/1.2-error.jpg)

## FAQ

### Deliverables

As a result of your work, please provide either a link to a git repository or
send a zip file with the code (don't forget to remove `node_modules` :)). 
The project should be runnable using `npm` scripts:

```
npm install
npm start:backend
npm start:frontend

# all systems ready!
```

### What language, framework, build tool... should I use?

Please use React and some boilerplate for your frontend. 
We recommend [Create React App](https://github.com/facebook/create-react-app), but
feel free to use anything you wish.

For the backend, again a boilerplate is a great idea, but you can go without it if you so wish.

## Useful Links

* [Bitbucket](https://bitbucket.org/) - Source code hosting, with free private repositories for small teams.
* [Google Fonts - Raleway](https://fonts.google.com/?selection.family=Raleway)
* [React](https://facebook.github.io/react/)
